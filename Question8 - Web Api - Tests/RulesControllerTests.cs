using Microsoft.AspNetCore.Mvc.Testing;
using Question8.Domain;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Question8.Tests
{
    public class RulesControllerTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> factory;

        public RulesControllerTests(WebApplicationFactory<Startup> factory)
        {
            this.factory = factory;
        }

        [Fact]
        public async Task PutNewAndGetIt()
        {
            var actualRule = new RuleForThings("baba", "you");

            await Put(actualRule);
            await GetExisting(actualRule);
        }

        private async Task Put(RuleForThings actualRule)
        {
            var putResponse = await factory.CreateClient().PutAsync($"rules/{actualRule.Thing}", actualRule, new JsonMediaTypeFormatter());
            await AssertReturnsRule(putResponse, actualRule.Thing, actualRule.Behavior);
        }

        private async Task GetExisting(RuleForThings actualRule)
        {
            var getResponse = await factory.CreateClient().GetAsync($"rules/{actualRule.Thing}");
            await AssertReturnsRule(getResponse, actualRule.Thing, actualRule.Behavior);
        }

        [Fact]
        public async Task PutNewAndPutExistingGetIt()
        {
            await Put(new RuleForThings("door", "shut"));

            var newRule = new RuleForThings("door", "melt");
            await Put(newRule);
            await GetExisting(newRule);
        }

        private static async Task AssertReturnsRule(HttpResponseMessage response, string expectedThing, string expectedBehavior)
        {
            if (response.StatusCode != HttpStatusCode.OK)
            {
                var responseAsString = await response.Content.ReadAsStringAsync();
                Assert.True(false, $"Expected 200 OK, got {(int)response.StatusCode} {response.StatusCode} with body : \n{responseAsString}");
            }

            Assert.InRange(response.Content.Headers.ContentLength ?? -1, 1, long.MaxValue);

            var rule = await response.Content.ReadAsAsync<RuleForThings>();

            Assert.Equal(expectedThing, rule.Thing);
            Assert.Equal(expectedBehavior, rule.Behavior);
        }

        [Fact]
        public async Task GetNotExisting_Returns404()
        {
            var response = await factory.CreateClient().GetAsync("rules/bug");

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task PutWithDifferentThing_Returns400()
        {
            var actualRule = new RuleForThings("key", "open");

            var response = await factory.CreateClient().PutAsync($"rules/box", actualRule, new JsonMediaTypeFormatter());

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
