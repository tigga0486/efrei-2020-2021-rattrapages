using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Question8.Domain;
using Question8.Repositories;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;

namespace Question8.Tests
{
    public class RulesControllerWithMockedRepositoryTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> factory;
        private readonly RuleRepository ruleRepository = new();

        public RulesControllerWithMockedRepositoryTests(WebApplicationFactory<Startup> factory)
        {
            this.factory = factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureTestServices(configure =>
                {
                    configure.AddSingleton<IRuleRepository>(ruleRepository);
                });
            });

            ruleRepository.StoreRule(new RuleForThings("baba", "you"));
            ruleRepository.StoreRule(new RuleForThings("wall", "stop"));
        }

        [Fact]
        public async Task GetExisting()
        {
            var response = await factory.CreateClient().GetAsync("rules/baba");

            await AssertReturnsRule(response, "baba", "you");
        }

        [Fact]
        public async Task PutNew()
        {
            var actualRule = new RuleForThings("door", "shut");

            var response = await factory.CreateClient().PutAsync($"rules/{actualRule.Thing}", actualRule, new JsonMediaTypeFormatter());

            await AssertReturnsRule(response, actualRule.Thing, actualRule.Behavior);
        }

        [Fact]
        public async Task PutExisting()
        {
            var actualRule = new RuleForThings("baba", "melt");

            var response = await factory.CreateClient().PutAsync($"rules/{actualRule.Thing}", actualRule, new JsonMediaTypeFormatter());

            await AssertReturnsRule(response, actualRule.Thing, actualRule.Behavior);
        }

        private static async Task AssertReturnsRule(HttpResponseMessage response, string expectedThing, string expectedBehavior)
        {
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.InRange(response.Content.Headers.ContentLength ?? -1, 1, long.MaxValue);

            var rule = await response.Content.ReadAsAsync<RuleForThings>();

            Assert.Equal(expectedThing, rule.Thing);
            Assert.Equal(expectedBehavior, rule.Behavior);
        }

        [Fact]
        public async Task GetNotExisting_Returns404()
        {
            var response = await factory.CreateClient().GetAsync("rules/bug");

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task PutWithDifferentThing_Returns400()
        {
            var actualRule = new RuleForThings("key", "open");

            var response = await factory.CreateClient().PutAsync($"rules/box", actualRule, new JsonMediaTypeFormatter());

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
