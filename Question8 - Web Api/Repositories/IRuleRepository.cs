﻿using Question8.Domain;

namespace Question8.Repositories
{
    public interface IRuleRepository
    {
        RuleForThings GetRule(string thing);
        void StoreRule(RuleForThings rule);
        void DeleteRule(string thing);
    }
}
